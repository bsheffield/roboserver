# Roboserver

Portable full stack RESTful application that uses Angular/FastAPI/Mongo/Node for tracking Robonation team messages.

## Getting Started

### Prerequisites

1. Docker Compose Installed
```shell
sudo snap install docker-compose
```

2. Set DNS for the hostname to resolve to IP of server this is running on: roboserver

**Limited example that does not use a DNS server. Used for local development/testing only.**
```shell
sudo cat "192.168.0.100 roboserver" >> /etc/hosts
```

### TLDR

Caution: Special attention must be placed on the .env file as it has all the credentials set for the containers like accessing the database.

Spin up all containers and see their output.
```shell
docker-compose up --build
```

* To view web interface, open web browser to `roboserver:8080`
* To view API docs, open web browser to `roboserver:8000/docs`


### Logs

See logs for all services
```shell
docker-compose logs -f
```

See logs for only the application service
```shell
docker-compose logs -f front-end
```

See logs for only the MongoDB service
```shell
docker-compose logs -f database
```

### Backend Development

Method 1: Local System

Requires Mongodb be running on host.

```bash
pip install -r requirements.txt
uvicorn app.main:app --host 0.0.0.0 --port 8000
```

Method 2: Docker

``` sh
docker build -t roboserver-backend .
```

Manually start the server for development. Requires Mongodb be running on host.

```sh
docker run --rm --network="host" -it roboserver-backend:latest /bin/bash
uvicorn app.main:app --host 0.0.0.0 --port 8000
```

### Frontend Development

Method 1: Local System

Assumes NodeJS is installed with npm accessible via commandline.

```bash
npm install -g @angular/cli
npm install --legacy-peer-deps
ng serve --host 0.0.0.0 --disable-host-check
```

Method 2: Docker


```bash
docker build -t roboserver-frontend .
docker run --rm --network="host" -p 8080:8080 -it roboserver-frontend /bin/bash
ng serve --host 0.0.0.0 --port 8080 --disable-host-check
```

import motor.motor_asyncio
from beanie import init_beanie
from core.config import MONGODB_URL

from db.models.gate import GateTask
from db.models.scan_code import ScanTheCodeTask
from db.models.follow_path import FollowThePathTask
from db.models.detect_dock import DetectAndDockTask
from db.models.find_fling import FindFlingTask
from db.models.heartbeat import HeartBeatMessage
from db.models.wildlife_encounter import WildLifeEncounterTask
from db.models.uav_replenishment import UAVReplenishmentTask
from db.models.uav_search_report import UAVSearchAndReportTask
from db.models.team import TeamModel

async def init_db():
    client = motor.motor_asyncio.AsyncIOMotorClient(
        MONGODB_URL
    )
    await init_beanie(database=client.roboserver_db,
                      document_models=[GateTask, ScanTheCodeTask, FollowThePathTask, DetectAndDockTask, FindFlingTask,
                                       HeartBeatMessage, WildLifeEncounterTask, UAVSearchAndReportTask,
                                       UAVReplenishmentTask,
                                       TeamModel
                                       ])

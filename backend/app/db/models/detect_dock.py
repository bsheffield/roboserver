from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class DetectAndDockTask(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXDOK,111221,161229,ROBOT,R,1*4E"
            }
        }

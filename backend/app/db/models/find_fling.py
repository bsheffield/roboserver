from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class FindFlingTask(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXFLG,111221,161229,ROBOT,R,2*40"
            }
        }

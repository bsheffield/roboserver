from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class FollowThePathTask(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXPTH,111221,161229,ROBOT,1*3C"
            }
        }

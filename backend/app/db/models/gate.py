from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class GateTask(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXGAT,111221,161229,ROBOT,1,2*3C"
            }}

from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class HeartBeatMessage(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXHRB,111221,161229,21.31198,N,157.88972,W,ROBOT,2,1*11"
            }
        }

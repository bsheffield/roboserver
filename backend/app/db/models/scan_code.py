from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class ScanTheCodeTask(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXCOD,111221,161229,ROBOT,RBG*5E"
            }
        }

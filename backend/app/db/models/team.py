import pymongo
from beanie import Document, Indexed
from typing import Optional
from datetime import datetime
from core.config import TEAMS_COLLECTION_NAME


class TeamModel(Document):
    message: str
    name: Optional[str] = ""
    date: Optional[str] = ""

    class Settings:
        name = TEAMS_COLLECTION_NAME

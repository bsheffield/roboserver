from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class UAVReplenishmentTask(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXUAV,111221,161229,ROBOT,2,1*2C"
            }
        }

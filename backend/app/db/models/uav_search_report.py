from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class UAVSearchAndReportTask(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXSAR,111221,161229,R,21.31198,N,157.88972,W,N, 21.32198,N,157.89972,W,ROBOT,2*0D"
            }
        }

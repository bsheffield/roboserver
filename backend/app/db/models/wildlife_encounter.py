from core.config import TEAMS_COLLECTION_NAME
from db.models.team import TeamModel


class WildLifeEncounterTask(TeamModel):
    message: str

    class Settings:
        name = TEAMS_COLLECTION_NAME

    class Config:
        schema_extra = {
            "example": {
                "message": "$RXENC,111221,161229,ROBOT,3,P,C,T*51"
            }
        }

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from db.database import init_db
from routes.heartbeat import router as heartbeat_router
from routes.gate import router as gates_router
from routes.follow_path import router as follow_path_router
from routes.wildlife_encounter import router as wildlife_router
from routes.scan_code import router as scan_code_router
from routes.detect_dock import router as detect_dock_router
from routes.find_fling import router as find_fling_router
from routes.uav_replenishment import router as uav_replenishment_router
from routes.uav_search_report import router as uav_search_report_router
from routes.teams import router as teams_router


app = FastAPI(title="Roboserver API", description="Interactive documentation for the Roboserver API. "
                                                  "Checkout the following drop-downs for example usage. "
                                                  "These examples can be modified and executed in the browser.",
              version="1.3.1")

app.include_router(teams_router, tags=["Teams"], prefix="/api")
app.include_router(heartbeat_router, tags=["Heartbeat"], prefix="/api")
app.include_router(gates_router, tags=["Entrance and Exit Gates"], prefix="/api")
app.include_router(follow_path_router, tags=["Follow the Path"], prefix="/api")
app.include_router(wildlife_router, tags=["Wildlife Encounter"], prefix="/api")
app.include_router(scan_code_router, tags=["Scan the Code"], prefix="/api")
app.include_router(detect_dock_router, tags=["Detect and Dock"], prefix="/api")
app.include_router(find_fling_router, tags=["Find and Fling"], prefix="/api")
app.include_router(uav_replenishment_router, tags=["UAV Replenishment"], prefix="/api")
app.include_router(uav_search_report_router, tags=["UAV Search and Report"], prefix="/api")


# Allow Cross-Domain. Note: very permissive for development purposes.
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"]
)

@app.on_event("startup")
async def startup():
    """Perform startup activities."""
    await init_db()

@app.on_event("shutdown")
async def shutdown():
    """Perform shutdown activities."""
    pass

@app.get("/", tags=["Root"])
async def root() -> dict:
    """
    Root page of API redirects to documentation page.
    :return:
    """

    return {"message": "Roboserver API. See /docs for API documentation"}

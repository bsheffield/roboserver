class Message(object):

    def __init__(self, data: str):
        self.msg_id: str = ""
        self.aedt_date: str = ""
        self.aedt_time: str = ""
        self.team_id: str = ""
        self.check_sum: str = ""

        self.data = data.split(",")
        if len(data) < 4:
            raise ValueError( "Length of message too short")

        self.msg_id = self.data[0]
        self.aedt_date = self.data[1]
        self.aedt_time = self.data[2]
        # team id is not consistent in handbook


class Heartbeat(Message):
    lat: str = ""
    ns_indicator: str = ""
    long: str = ""
    ew_indicator: str = ""
    system_mode: str = ""
    uav_status: str = ""

    def __init__(self, data: str):
        super().__init__(data)

        msg_len = len(self.data)
        if msg_len < 10:
            raise ValueError("Length of message too short")

        if msg_len > 10:
            raise ValueError("Length of message too long")

        self.lat = self.data[3]
        self.ns_indicator = self.data[4]
        self.long = self.data[5]
        self.ew_indicator = self.data[6]
        self.team_id = self.data[7]
        self.system_mode = self.data[8]
        end_of_msg = self.data[9].split("*")
        if len(end_of_msg) != 2:
            raise ValueError("End of message not of expected length. Has the checksum been added?")
        self.uav_status = end_of_msg[0]
        self.check_sum = end_of_msg[1]


class Gate(Message):
    entrance_gate: str = ""
    exit_gate: str = ""

    def __init__(self, data: str):
        super().__init__(data)

        msg_len = len(self.data)
        if msg_len < 6:
            raise ValueError( "Length of message too short")

        if msg_len > 6:
            raise ValueError( "Length of message too long")

        self.team_id = self.data[3]
        self.active_entrance_gate = self.data[4]
        self.active_exit_gate = self.data[5]

        end_of_msg = self.data[5].split("*")
        if len(end_of_msg) != 2:
            raise ValueError("End of message not of expected length. Has the checksum been added?")
        self.active_exit_gate = end_of_msg[0]
        self.check_sum = end_of_msg[1]


class FollowPath(Message):
    finished: str = ""

    def __init__(self, data: str):
        super().__init__(data)
        msg_len = len(self.data)
        if msg_len < 5:
            raise ValueError("Length of message too short")

        if msg_len > 5:
            raise ValueError("Length of message too long")

        self.team_id = self.data[3]

        end_of_msg = self.data[4].split("*")
        if len(end_of_msg) != 2:
            raise "End of message not of expected length. Has the checksum been added?"
        self.finished = end_of_msg[0]
        self.check_sum = end_of_msg[1]


class WildLifeEncounter(Message):
    num_detected: str = ""
    first_wildlife: str = ""
    second_wildlife: str = ""
    third_wildlife: str = ""

    def __init__(self, data: str):
        super().__init__(data)

        msg_len = len(self.data)
        if msg_len < 8:
            raise ValueError("Length of message too short")

        if msg_len > 8:
            raise ValueError("Length of message too long")

        self.team_id = self.data[3]
        self.num_detected = self.data[4]
        self.first_wildlife = self.data[5]
        self.second_wildlife = self.data[6]

        end_of_msg = self.data[7].split("*")
        if len(end_of_msg) != 2:
            raise ValueError("End of message not of expected length. Has the checksum been added?")
        self.third_wildlife = end_of_msg[0]
        self.check_sum = end_of_msg[1]


class ScanTheCode(Message):
    light_pattern: str = ""

    def __init__(self, data: str):
        super().__init__(data)
        msg_len = len(self.data)
        if msg_len < 5:
            raise ValueError("Length of message too short")

        if msg_len > 5:
            raise ValueError("Length of message too long")

        self.team_id = self.data[3]

        end_of_msg = self.data[4].split("*")
        if len(end_of_msg) != 2:
            raise ValueError("End of message not of expected length. Has the checksum been added?")
        self.light_pattern = end_of_msg[0]
        self.check_sum = end_of_msg[1]


class DetectDock(Message):
    color: str = ""
    ams_status: str = ""

    def __init__(self, data: str):
        super().__init__(data)
        msg_len = len(self.data)
        if msg_len < 6:
            raise ValueError("Length of message too short")

        if msg_len > 6:
            raise ValueError("Length of message too long")

        self.team_id = self.data[3]
        self.color = self.data[4]

        end_of_msg = self.data[5].split("*")
        if len(end_of_msg) != 2:
            raise ValueError("End of message not of expected length. Has the checksum been added?")
        self.ams_status = end_of_msg[0]
        self.check_sum = end_of_msg[1]


class FindFling(Message):
    color: str = ""
    ams_status: str = ""

    def __init__(self, data: str):
        super().__init__(data)
        msg_len = len(self.data)
        if msg_len < 6:
            raise ValueError("Length of message too short")

        if msg_len > 6:
            raise ValueError("Length of message too long")

        self.team_id = self.data[3]
        self.color = self.data[4]

        end_of_msg = self.data[5].split("*")
        if len(end_of_msg) != 2:
            raise ValueError("End of message not of expected length. Has the checksum been added?")
        self.ams_status = end_of_msg[0]
        self.check_sum = end_of_msg[1]


class UAVReplenishment(Message):
    uav_status: str = ""
    item_status: str = ""

    def __init__(self, data: str):
        super().__init__(data)
        msg_len = len(self.data)
        if msg_len < 6:
            raise ValueError("Length of message too short")

        if msg_len > 6:
            raise ValueError("Length of message too long")

        self.team_id = self.data[3]
        self.uav_status = self.data[4]

        end_of_msg = self.data[5].split("*")
        if len(end_of_msg) != 2:
            raise ValueError("End of message not of expected length. Has the checksum been added?")
        self.item_status = end_of_msg[0]
        self.check_sum = end_of_msg[1]


class UAVSearchandReport(Message):
    obj_reported_1: str = ""
    obj_lat_1: str = ""
    ns_indicator_1: str = ""
    obj_long_1: str = ""
    ew_indicator_1: str = ""

    obj_reported_2: str = ""
    obj_lat_2: str = ""
    ns_indicator_2: str = ""
    obj_long_2: str = ""
    ew_indicator_2: str = ""

    uav_status: str = ""

    def __init__(self, data: str):
        super().__init__(data)
        msg_len = len(self.data)
        if msg_len < 15:
            raise ValueError("Length of message too short")

        if msg_len > 15:
            raise ValueError("Length of message too long")

        self.obj_reported_1 = self.data[3]
        self.obj_lat_1 = self.data[4]
        self.ns_indicator_1 = self.data[5]
        self.obj_long_1 = self.data[6]
        self.ew_indicator_1 = self.data[7]

        self.obj_reported_2 = self.data[8]
        self.obj_lat_2 = self.data[9]
        self.ns_indicator_2 = self.data[10]
        self.obj_long_2 = self.data[11]
        self.ew_indicator_2 = self.data[12]

        self.team_id = self.data[13]

        end_of_msg = self.data[14].split("*")

        if len(end_of_msg) != 2:
            raise ValueError("End of message not of expected length. Has the checksum been added?")
        self.uav_status = end_of_msg[0]
        self.check_sum = end_of_msg[1]

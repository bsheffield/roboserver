from datetime import datetime
from fastapi import APIRouter, HTTPException
from typing import List
from db.models.detect_dock import DetectAndDockTask
from messages.messages import DetectDock
from db.models.team import TeamModel

router = APIRouter()


@router.post("/detect_dock", response_description="Creates a team entry for the Detect and Dock task")
async def create_team_entry(team_entry: DetectAndDockTask) -> dict:
    data = team_entry.message

    try:
        msg = DetectDock(data)
        team_entry.name = msg.team_id
        team_entry.date = str(datetime.now())
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))

    await team_entry.create()

    return {"message": "Success"}


@router.get("/detect_dock", response_description="List team data for the Detect and Dock task")
async def get_report() -> List[TeamModel]:
    """
    Returns team specific data from the database for the docking task
    :return: The latest team entries of data recorded for the docking task
    """

    teams = await DetectAndDockTask.find().to_list(50)
    return teams

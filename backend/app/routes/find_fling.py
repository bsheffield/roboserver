from datetime import datetime
from fastapi import APIRouter, HTTPException
from typing import List
from db.models.find_fling import FindFlingTask
from messages.messages import FindFling

router = APIRouter()

@router.post("/find_fling", response_description="Creates a team entry for the Find and Fling task")
async def create_team_entry(team_entry: FindFlingTask) -> dict:

    data = team_entry.message

    try:
        msg = FindFling(data)
        team_entry.name = msg.team_id
        team_entry.date = str(datetime.now())
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))

    await team_entry.create()

    return {"message": "Success"}

@router.get("/find_fling", response_description="List team data for theFind and Fling task")
async def get_report() -> List[FindFlingTask]:

    teams = await FindFlingTask.find().to_list(50)
    return teams

from datetime import datetime
from fastapi import APIRouter, HTTPException
from typing import List
from db.models.follow_path import FollowThePathTask
from messages.messages import FollowPath

router = APIRouter()

@router.post("/follow_path", response_description="Creates a team entry for the Follow the Path task")
async def create_team_entry(team_entry: FollowThePathTask) -> dict:

    data = team_entry.message

    try:
        msg = FollowPath(data)
        team_entry.name = msg.team_id
        team_entry.date = str(datetime.now())
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))

    await team_entry.create()

    return {"message": "Success"}

@router.get("/follow_path", response_description="List team data for the Follow the Path task")
async def get_report() -> List[FollowThePathTask]:

    teams = await FollowThePathTask.find().to_list(50)
    return teams


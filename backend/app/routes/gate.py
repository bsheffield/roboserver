from datetime import datetime
from fastapi import APIRouter, HTTPException
from typing import List
from db.models.gate import GateTask
from messages.messages import Gate

router = APIRouter()

@router.post("/gate", response_description="Creates a team entry for the Entrance/Exit Gate task")
async def create_team_entry(team_entry: GateTask) -> dict:

    data = team_entry.message

    try:
        msg = Gate(data)
        team_entry.name = msg.team_id
        team_entry.date = str(datetime.now())
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))

    await team_entry.create()

    return {"message": "Success"}

@router.get("/gate", response_description="List team data for the Entrance/Exit Gate task")
async def get_report() -> List[GateTask]:

    teams = await GateTask.find().to_list(50)
    return teams



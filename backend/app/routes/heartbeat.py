from datetime import datetime
from fastapi import APIRouter, HTTPException
from typing import List
from db.models.heartbeat import HeartBeatMessage
from messages.messages import Heartbeat

router = APIRouter()

@router.post("/heartbeat", response_description="Creates a team entry for reported heartbeat")
async def create_team_entry(team_entry: HeartBeatMessage) -> dict:

    data = team_entry.message

    try:
        msg = Heartbeat(data)
        team_entry.name = msg.team_id
        team_entry.date = str(datetime.now())
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))

    await team_entry.create()

    return {"message": "Success"}

@router.get("/heartbeat", response_description="List team data that reported heartbeats")
async def get_report() -> List[HeartBeatMessage]:
    """
    Returns team specific data from the database that reported heartbeats
    :return: The latest team entries of data recorded for the docking task
    """

    teams = await HeartBeatMessage.find().to_list(50)
    return teams


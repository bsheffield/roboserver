from fastapi import APIRouter
from typing import List
from db.models.team import TeamModel
router = APIRouter()

@router.get("/teams/names", response_description="List team names")
async def get_team_names() -> List[TeamModel]:

    team_names = await TeamModel.distinct(TeamModel.name)
    teams = list()
    for t in team_names:
        # These are filler values as this data is not actually stored in the database.
        team_entry = TeamModel(message="", name=t)
        teams.append(team_entry)

    return teams

@router.get("/teams", response_description="Lists team data for all tasks by most recent")
async def get_teams() -> List[TeamModel]:

    teams = await TeamModel.find().sort("-date").to_list(15)
    return teams

@router.get("/teams/{name}", response_description="List team specific data")
async def get_team_by_name(name: str) -> List[TeamModel]:
    """
    Returns team specific data from the database
    :param name: The team name
    :return: The latest team entries of data recorded for the specific team.
    """

    teams = await TeamModel.find(TeamModel.name == name).to_list()
    return teams

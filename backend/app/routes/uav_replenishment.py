from datetime import datetime

from fastapi import APIRouter, HTTPException
from typing import List
from db.models.uav_replenishment import UAVReplenishmentTask
from messages.messages import UAVReplenishment

router = APIRouter()

@router.post("/uav_replenishment", response_description="Creates a team entry for the UAV Replenishment task")
async def create_team_entry(team_entry: UAVReplenishmentTask) -> dict:
    """

    :param team_entry:
    :return:
    """

    data = team_entry.message

    try:
        msg = UAVReplenishment(data)
        team_entry.name = msg.team_id
        team_entry.date = str(datetime.now())
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))

    await team_entry.create()

    return {"message": "Success"}


@router.get("/uav_replenishment", response_description="List team data for the UAV Replenishment task")
async def get_report() -> List[UAVReplenishmentTask]:


    teams = await UAVReplenishmentTask.find().to_list(50)
    return teams



from datetime import datetime

from fastapi import APIRouter, HTTPException
from typing import List
from db.models.uav_search_report import UAVSearchAndReportTask
from messages.messages import UAVSearchandReport

router = APIRouter()

@router.post("/uav_search", response_description="Creates a team entry for the UAV Search and Report task")
async def create_team_entry(team_entry: UAVSearchAndReportTask) -> dict:

    data = team_entry.message

    try:
        msg = UAVSearchandReport(data)
        team_entry.name = msg.team_id
        team_entry.date = str(datetime.now())
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))

    await team_entry.create()

    return {"message": "Success"}

@router.get("/uav_search", response_description="List team data for the task")
async def get_report() -> List[UAVSearchAndReportTask]:
    """
    Returns team specific data from the database for the task
    :return: The latest team entries of data recorded for the task
    """

    teams = await UAVSearchAndReportTask.find().to_list(50)
    return teams


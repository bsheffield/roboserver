from datetime import datetime

from fastapi import APIRouter, HTTPException
from typing import List
from db.models.wildlife_encounter import WildLifeEncounterTask
from messages.messages import WildLifeEncounter

router = APIRouter()

@router.post("/wildlife_encounter", response_description="Creates a team entry for the Wildlife Encounter task")
async def create_team_entry(team_entry: WildLifeEncounterTask) -> dict:

    data = team_entry.message

    try:
        msg = WildLifeEncounter(data)
        team_entry.name = msg.team_id
        team_entry.date = str(datetime.now())
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))

    await team_entry.create()

    return {"message": "Success"}

@router.get("/wildlife_encounter", response_description="List team data for the Wildlife Encounter task")
async def get_report() -> List[WildLifeEncounterTask]:


    teams = await WildLifeEncounterTask.find().to_list(50)
    return teams


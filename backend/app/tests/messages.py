import unittest
import requests

headers = {
    'accept': 'application/json',
}

class TestCase(unittest.TestCase):
    """
    Test POST messages assuming backend is running.
    """

    def test_heartbeat(self):
        json_data = {
         "message": "$RXHRB,111221,161229,21.31198,N,157.88972,W,ROBOT,2,1*11"
        }

        response = requests.post('http://localhost:8000/api/heartbeat', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)

    def test_gates(self):
        json_data = {
            "message": "$RXGAT,111221,161229,ROBOT,1,2*3C"
        }

        response = requests.post('http://localhost:8000/api/gate', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)

    def test_follow_path(self):
        json_data = {
            "message": "$RXPTH,111221,161229,ROBOT,1*3C"
        }

        response = requests.post('http://localhost:8000/api/follow_path', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)

    def test_wildlife(self):
        json_data = {
            'message': '$RXENC,111221,161229,ROBOT,3,P,C,T*51',
        }

        response = requests.post('http://localhost:8000/api/wildlife_encounter', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)

    def test_scan(self):
        json_data = {
            "message": "$RXCOD,111221,161229,ROBOT,RBG*5E"
        }

        response = requests.post('http://localhost:8000/api/scan_code', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)

    def test_detect_dock(self):
        json_data = {
            "message": "$RXDOK,111221,161229,ROBOT,R,1*4E"
        }

        response = requests.post('http://localhost:8000/api/detect_dock', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)

    def test_find_fling(self):
        json_data = {
            "message": "$RXFLG,111221,161229,ROBOT,R,2*40"
        }

        response = requests.post('http://localhost:8000/api/find_fling', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)

    def test_uav_replenishment(self):
        json_data = {
            "message": "$RXUAV,111221,161229,ROBOT,2,1*2C"
        }

        response = requests.post('http://localhost:8000/api/uav_replenishment', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)

    def test_uav_search(self):
        json_data = {
            "message": "$RXSAR,111221,161229,R,21.31198,N,157.88972,W,N, 21.32198,N,157.89972,W,ROBOT,2*0D"
        }

        response = requests.post('http://localhost:8000/api/uav_search', headers=headers, json=json_data)
        self.assertEqual({'message': 'Success'}, response.json())
        self.assertEqual(200, response.status_code)



export interface Team {
  name: string;
  message: string;
  date: string;
}
